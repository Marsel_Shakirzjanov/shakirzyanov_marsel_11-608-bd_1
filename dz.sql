﻿CREATE TABLE Fabric 
(id int PRIMARY KEY, 
name text, 
salary integer, 
manager_id integer, 
departament_id integer 
); 
INSERT INTO Fabric
(id,name,salary ) 
values (1,'Николай',100),(2,'Артем',200),(3,'Валера',10000),(4,'Марсель',374),(5,'Саша',6334),(6,'Федор',987987); 
SELECT * FROM Fabric;
DELETE FROM Fabric WHERE name = 'Артем'; 
SELECT name FROM Fabric as emp WHERE salary > 500; 
UPDATE Fabric SET manager_id = 6 WHERE name = 'Николай'; 
SELECT * FROM Fabric;